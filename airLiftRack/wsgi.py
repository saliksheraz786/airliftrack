"""
WSGI config for airLiftRack project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "airLiftRack.settings")

application = get_wsgi_application()


from administrator.cloud import pullCloudData
from administrator.tasks import updateCompartmentStates

pullCloudData()
# updateCompartmentStates()
