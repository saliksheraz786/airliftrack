from django.db import models


class Compartments(models.Model):
    compartment = models.CharField(max_length=20, null=True, blank=True)
    code = models.IntegerField(null=True, blank=True)
    time = models.CharField(max_length=50, null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    boxstate = models.CharField(max_length=1, null=True, blank=True)
    lastUpdated = models.DateTimeField(null=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.compartment + " | Status: " + str(self.status)


class temp(models.Model):
    compartment = models.CharField(max_length=20, null=True, blank=True)
    lastUpdated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.compartment + " | " + str(self.lastUpdated)


class Counter(models.Model):
    api = models.IntegerField(default=0)

    def __str__(self):
        return str(self.api)

class CounterCompartments(models.Model):
    num = models.IntegerField(default=0)

    def __str__(self):
        return str(self.api)