import requests
from .models import Compartments
from administrator.models import Counter, CounterCompartments

# This function will run as the server will be started, it is called in wsgi.py file of main project folser.
def pullCloudData():
    try:
        cloudResponse = requests.get(
            "https://api.airliftgrocer.com/v2/orders/packed/compartments?warehouse=386", headers={"auth": "Groc3R@Sm@rtR@ck"}
        ).json()
        # print(cloudResponse)
        # allData = Compartments.objects.all()
        # for each in allData:
        #     each.delete()
        for each in cloudResponse:
            compartmentObj = Compartments.objects.filter(compartment=each["compartment"])
            if compartmentObj:
                compartmentObj = compartmentObj[0]
                compartmentObj.code = each["code"]
                compartmentObj.time = each["time"]
                compartmentObj.duration = each["duration"]
                compartmentObj.boxstate = each["boxstate"]
                compartmentObj.save()
                # print("Compartment: " + str(each["compartment"]) + " | Updated ! , Data = " + str(each))
            else:
                Compartments.objects.create(
                    compartment=each["compartment"],
                    code=each["code"],
                    time=each["time"],
                    duration=each["duration"],
                    boxstate=each["boxstate"],
                    status=False,
                )
                # print("New Compartment:" + str(each["compartment"]) + " Added | Created ! , Data = " + str(each))
        print("Data Pulled")
        return True
    except:
        return False


def pullCloudDataAfterDelay(delay):
    counterObj = Counter.objects.first()
    if not counterObj:
        counterObj = Counter.objects.create()
    counterObj.api = counterObj.api + 1
    counterObj.save()
    counter = counterObj.api
    if counter % delay == 1:
        pullCloudData()
    return False



