from .models import Compartments
from datetime import datetime, timezone
from .models import CounterCompartments


def updateCompartmentStates():
    currentTime = datetime.now(timezone.utc)
    allCompartments = Compartments.objects.all()
    for eachCompartment in allCompartments:
        if eachCompartment.lastUpdated:
            diff = currentTime - eachCompartment.lastUpdated
            if diff.seconds > 15:
                eachCompartment.status = False
                eachCompartment.save()
            # print(diff.seconds)
        # print("Compartment: " + str(eachCompartment.compartment) + " | Status: " + str(eachCompartment.status))
    return True


def updateCompartmentsAfterDelay(delay):
    counterObj = CounterCompartments.objects.first()
    if not counterObj:
        counterObj = CounterCompartments.objects.create()
    counterObj.num = counterObj.num + 1
    counterObj.save()
    counter = counterObj.num
    if counter % delay == 1:
        updateCompartmentStates()
    return False
