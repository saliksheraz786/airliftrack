from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Compartments
from datetime import datetime, timezone
from .tasks import updateCompartmentStates, updateCompartmentsAfterDelay
from .cloud import pullCloudData, pullCloudDataAfterDelay


def index(request):
    if not pullCloudData():
        context = {"error": "Internet Down"}
        return render(request, "administrator/index.html", context)
    updateCompartmentStates()
    allCompartments = Compartments.objects.all().order_by("compartment")
    context = {"allCompartments": allCompartments}
    return render(request, "administrator/index.html", context)


def compartmentData(request):
    currentTime = datetime.now(timezone.utc)
    if request.method == "GET":
        compartment = request.GET.get("compartment")
        if not compartment:
            return JsonResponse({}, status=404)
        compartmentObj = Compartments.objects.get(compartment=compartment)
        compartmentObj.lastUpdated = currentTime
        compartmentObj.status = True
        compartmentObj.save()
        pullCloudDataAfterDelay(80)
        updateCompartmentsAfterDelay(100)

        data = {
            "compartment": str(compartmentObj.compartment),
            "code": str(compartmentObj.code),
            "time": str(compartmentObj.time),
            "duration": str(compartmentObj.duration),
            "boxstate": str(compartmentObj.boxstate),
        }
        return JsonResponse(data)
